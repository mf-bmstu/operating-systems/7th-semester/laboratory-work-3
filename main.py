# время выполнения запроса = время поиска + время ожидания нужного сектора + время чтения или записи
# время поиска = время перехода с одной дорожки на соседнюю (5мс) * на количество дорожек между текущей и нужной + 1)
#
# время ожидания = время простоя головки над нужной дорожкой в ожидании начала запрощенного сектора
# время простая = (необходимое положение головки (в градусах) - текующее положение головки (в градусах)) * скорость вращение (относительно градусов)
# необходимое положение головки = 360 / общее количество секторов * номер запрощенного сектора
# текущее положение головки = (время прошедшее от начала работы диска * скорость диска) % 360
# время прощедшее от начала работы диска до начала времени простоя = суммарное время выполнения каждого запроса + сумма интервалов между запросами
#
#
# время чтения = количество считываемых секторов * скорость чтения одного сетктора
# время записи = количество записываемых секторов * скорость чтения одного сетктора + время за которое диск совершает полный оброт
#
# скорость вращения диска = 1 оборот \ 6 мс = 60 градусов \ 1 мс
# скорость чтения одного сектора =  время за которое диск совершает полный оброт \ количество секторов


from random import randint
from pprint import pprint

OPERATION = ["r", "w"]


class Stats:
    def __init__(self):
        self.downtime = 0
        self.uptime = 0
        self.min_query_execution_time = 0
        self.max_query_execution_time = 0
        self.avg_query_execution_time = 0
        self.standard_deviation_query_execution_time = 0
        self.maximum_queue_length = 0
        self.querys_times = list()

    def print_stats(self):
        print(f"Общее время исполнения: {self.downtime + self.uptime} \n"
              f"Послезное время: {self.uptime} \n"
              f"Время в простое: {self.downtime} \n"
              f"Количество выполненых запросов: {len(self.querys_times)} \n"
              f"Максимальная длителность выполнения запроса: {max(self.querys_times)} \n"
              f"Минимальная длителность выполнения запроса: {min(self.querys_times)} \n"
              f"Средняя длителность выполнения запроса: {sum(self.querys_times) / len(self.querys_times)} \n"
              f"Среднеквадратическое отклонение длителности выполнения запроса: {self.standard_deviation_query_execution_time_calc()} \n"
              f"Максимальная длинна очереди: {self.maximum_queue_length}\n")
        print()

    def standard_deviation_query_execution_time_calc(self):
        mean = sum(self.querys_times) / len(self.querys_times)
        variance = sum([((x - mean) ** 2) for x in self.querys_times]) / len(self.querys_times)
        res = variance ** 0.5
        return res


class Disk:
    def __init__(self, rotation_speed, track_transition_time, sectors_amount, tracks_amount, surfaces_amount):
        self.rotation_speed = rotation_speed  # количество обротов в 1 мс
        self.track_transition_time = track_transition_time
        self.sectors = sectors_amount
        self.tracks = tracks_amount
        self.surfaces = surfaces_amount
        self.time = 0
        self.current_track = 0


class Query:
    def __init__(self, first_sector, last_sector, track_number, surface_number, operation_type, receipt_time):
        self.first_sector = first_sector  # Сектора нумеруются от 0 до disk.sectors - 1
        self.last_sector = last_sector
        self.track_number = track_number
        self.surface_number = surface_number
        self.operation_type = operation_type
        self.receipt_time = receipt_time


def generate_query(disk, receipt_time):
    query = Query(
        randint(1, disk.sectors),
        randint(1, disk.sectors),
        randint(1, disk.tracks),
        randint(1, disk.surfaces),
        OPERATION[randint(0, len(OPERATION) - 1)],
        receipt_time
    )
    return query


def print_query_queue(querys, disk):
    print(f"Текущий трек диска: {disk.current_track} Количетво оставшихся запросов: {len(querys)}")

    if len(querys) == 0 or querys[0].receipt_time > disk.time:
        print("Очередь пуста")
        return

    current_query_index = 0
    print("Очередь: ", end='')
    while querys[current_query_index].receipt_time < disk.time:
        print(f"[{current_query_index}]:{querys[current_query_index].track_number} ", end='')

        current_query_index = current_query_index + 1
        if current_query_index >= len(querys):
            break
    print(f"\nДлина очереди: {current_query_index}\n")


def select_query_fifo(querys):
    query = querys[0]
    querys.pop(0)
    return query


def select_query_sstf(querys, disk):
    if querys[0].receipt_time >= disk.time:
        query = querys[0]
        querys.pop(0)
        return query, 0, None

    min_distance = abs(disk.current_track - querys[0].track_number)
    nearest_query_index = 0
    current_query_index = 0

    while querys[current_query_index].receipt_time < disk.time:
        if abs(disk.current_track - querys[current_query_index].track_number) < min_distance:
            min_distance = abs(disk.current_track - querys[current_query_index].track_number)
            nearest_query_index = current_query_index

        # print(f"Ближайший: {nearest_query_index}, Текущий [{current_query_index}]:{querys[current_query_index].track_number}, Минимальная дистанция {min_distance}, Текущая дистанция {abs(disk.current_track - querys[current_query_index].track_number)}")
        current_query_index = current_query_index + 1
        if current_query_index >= len(querys):
            break

    query = querys[nearest_query_index]
    querys.pop(nearest_query_index)
    return query, nearest_query_index, min_distance


# В очереди находяться только те запросы чьё время поступления меньше текущего времени
# После исполнения запроса его длительность добавляется к текущему времени
# При выборе нового запроса возможно два варианта:
#   1. Время поступления запроса меньше текущего, тогда текущее время никак не изменяется
#   (Запрос сразу же начинает исполняться)
#   2. Время поступления запроса больше текущего, тогда текущее время устанавливается равным времени поступления запроса
#   (Запрос начинает испольняться только тогда когда он поступил)
def execute_query(disk, selector_type, querys, stats):
    # print_query_queue(querys, disk)

    current_query_index = 0
    if len(querys) > 0:
        while querys[current_query_index].receipt_time < disk.time:
            current_query_index = current_query_index + 1
            if current_query_index >= len(querys):
                break

    if stats.maximum_queue_length < current_query_index:
        stats.maximum_queue_length = current_query_index

    query = None
    match selector_type:
        case 'fifo':
            query = select_query_fifo(querys)
        case 'sstf':
            query, index, distance = select_query_sstf(querys, disk)
            # print(f"SSTF selected: {index}, distance: {distance}\n\n\n")

    if query.receipt_time > disk.time:
        stats.downtime = stats.downtime + query.receipt_time - disk.time
        disk.time = query.receipt_time

    query_execution_time = calculate_query_execution_time(query, disk)

    disk.current_track = query.track_number

    stats.querys_times.append(query_execution_time)
    stats.uptime = stats.uptime + query_execution_time

    disk.time = disk.time + query_execution_time


def calculate_query_execution_time(query, disk):
    # время поиска = количество дорожек между текущей и нужной * время перехода с одной дорожки на соседнюю
    find_time = abs(disk.current_track - query.track_number) * disk.track_transition_time

    if query.last_sector - query.first_sector + 1 > 0:
        sector_amount_for_operation = query.last_sector - query.first_sector + 1
    else:
        sector_amount_for_operation = (query.last_sector - query.first_sector + disk.sectors) + 1

    # Расчёт в обортах в мс
    disired_head_position = (query.first_sector / disk.sectors)
    current_head_position = ((disk.time + find_time) * disk.rotation_speed) % 1
    if disired_head_position - current_head_position > 0:
        angular_distance = disired_head_position - current_head_position
    else:
        angular_distance = disired_head_position - current_head_position + 1
    wait_time = angular_distance / disk.rotation_speed

    # Время за которое соверщается один оборот (при 10 оборотах в мс равно 1/10 мс) делённое на количество секторов
    sector_transit_time = 1 / disk.rotation_speed / disk.sectors
    operation_time = None
    match query.operation_type:
        case 'r':
            # время чтения = количество считываемых секторов * скорость чтения одного сетктора
            operation_time = sector_amount_for_operation * sector_transit_time
        case 'w':
            # время записи = количество записываемых секторов * скорость чтения одного сетктора + время за которое
            # диск совершает полный оброт
            operation_time = sector_amount_for_operation * sector_transit_time + 1 / disk.rotation_speed

    full_query_time = find_time + wait_time + operation_time
    return full_query_time


if __name__ == '__main__':
    # Нужно вывести максимально, минимальное, среднеее время запроса
    max_query_iterval_time = 100
    simulation_time = 5 * 60000

    disk1 = Disk(rotation_speed=10000 / 1000,
                 track_transition_time=0.5,
                 sectors_amount=16,
                 tracks_amount=500,
                 surfaces_amount=4
                 )

    stats1 = Stats()

    querys_buffer1 = list()

    time = 0
    while time < simulation_time:
        time = time + randint(1, max_query_iterval_time)
        querys_buffer1.append(generate_query(disk1, time))

    while disk1.time < simulation_time and len(querys_buffer1) > 0:
        execute_query(disk1, 'fifo', querys_buffer1, stats1)

    print("Статиска для стратегии FIFO")
    stats1.print_stats()

    disk2 = Disk(rotation_speed=10000 / 1000,
                 track_transition_time=0.5,
                 sectors_amount=16,
                 tracks_amount=500,
                 surfaces_amount=4
                 )

    stats2 = Stats()

    querys_buffer2 = list()

    time = 0
    while time < simulation_time:
        time = time + randint(1, max_query_iterval_time)
        querys_buffer2.append(generate_query(disk2, time))

    while disk2.time < simulation_time and len(querys_buffer2) > 0:
        execute_query(disk2, 'sstf', querys_buffer2, stats2)

    print("Статиска для стратегии SSTF")
    stats2.print_stats()
